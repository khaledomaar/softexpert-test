//
//  APIs.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/21/21.
//

import Foundation

class APIs {
    static let Instance = APIs()
    private init() {}
    private let app_id = "2a052ac5"
    private let app_key = "1c9d15706f8bc7502ae63bcecdd34de0"
    private let url = "https://api.edamam.com/search?"

    //MARK:- Apis

    func getRecipesSearchResult(searchKey: String, from: Int, to: Int) -> String {
        return url + "q=\(searchKey)&app_id=\(app_id)&app_key=\(app_key)&from=\(from)&to=\(to)"
    }
}
