//
//  APIClient.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/21/21.
//

import Foundation

enum HTTPMethod: String{
    case get = "GET"
    case post = "POST"
}

enum APIClient {
    static func request<ResponseType: Decodable>(url: URL, httpMethod: HTTPMethod, responseType: ResponseType.Type, completion: @escaping (ResponseType?, Error?) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        let task = URLSession.shared.dataTask(with: request) {data, response, error in
            var completionError: Error?
            var completionResponse: ResponseType?
            guard let data = data, error == nil else {
                completionError = error
                return
            }
            do {
                let decodedResponse = try JSONDecoder().decode(ResponseType.self, from: data)
                completionResponse = decodedResponse
            } catch let error {
                completionError = error
            }
            DispatchQueue.main.async {
                completion(completionResponse, completionError)
            }
        }
        task.resume()
    }
}
