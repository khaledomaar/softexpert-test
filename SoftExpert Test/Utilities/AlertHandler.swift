//
//  AlertHandler.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/22/21.
//

import Foundation
import UIKit

class AlertHandler {
    private weak var presentingViewCtrl: UIViewController?

    init(presentingViewCtrl: UIViewController) {
        self.presentingViewCtrl = presentingViewCtrl
    }

    func showErrorMessage(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
        presentingViewCtrl?.present(alert, animated: true)
    }
}
