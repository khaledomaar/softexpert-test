//
//  ActivityIndicator.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/21/21.
//

import JGProgressHUD

class ActivityIndicator {
    var progressHud: JGProgressHUD

    init() {
        progressHud = JGProgressHUD()
    }

    func displayForLoading(in view: UIView) {
        progressHud = JGProgressHUD(style: .dark)
        progressHud.textLabel.text = "Loading"
        progressHud.show(in: view)
    }

    func dismiss() {
        progressHud.dismiss(animated: true)
    }
}
