//
//  UITableViewExt.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/21/21.
//

import UIKit

extension UITableView {
    func dequeCell<Cell: UITableViewCell> () -> Cell {
        let cellName = String(describing: Cell.self)
        guard let myCell = dequeueReusableCell(withIdentifier: cellName) as? Cell else { return Cell() }
        return myCell
    }
    
    func setEmptyView(message: String) {
        let messageLabel = UILabel()
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont.systemFont(ofSize: 20)
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        backgroundView = messageLabel        
    }

    func removeEmptyState() {
        backgroundView = nil
    }
    
    func startSpinnerIndicator() {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.bounds.width, height: CGFloat(44))
        self.tableFooterView = spinner
        self.tableFooterView?.isHidden = false
    }
}
