//
//  UIViewExt.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/21/21.
//

import UIKit

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    func setShadow() {
        layer.cornerRadius = 7
        layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        layer.shadowRadius = 10
        layer.shadowOffset = .zero
        layer.shadowOpacity = 1
        layer.masksToBounds = false
    }
}

