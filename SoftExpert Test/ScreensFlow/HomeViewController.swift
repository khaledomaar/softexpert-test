//
//  HomeViewController.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/24/21.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func goToRecipeSearch(_ sender: Any) {
        let searchRecipeVC = storyboard?.instantiateViewController(withIdentifier: "RecipeSearchViewController") as! RecipeSearchViewController
        self.navigationController?.pushViewController(searchRecipeVC, animated: true)
    }
}
