//
//  RecipesListModel.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/21/21.
//

import Foundation

struct RecipesListModel: Decodable {
    var q: String?
    var from: Int?
    var to: Int?
    var more: Bool?
    var count: Int?
    var hits: [Recipe]?
}

struct Recipe: Decodable{
    var recipe: RecipeData?
}

struct RecipeData: Decodable{
    private enum CodingKeys: String, CodingKey {
        case image, source, healthLabels, ingredientLines
        case title = "label"
        case recipeUrl = "url"
    }
    var image: String?
    var title: String?
    var source: String?
    var recipeUrl: String?
    var healthLabels: [String]?
    var ingredientLines: [String]?
}
