//
//  RecipesSearchViewModel.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/21/21.
//

import Foundation

class RecipesSearchViewModel {
    //MARK: -  clousers

    var errorHandler: ((_: String) -> Void)?
    var newDataToDisplayHandler: (() -> Void)?
    //MARK: -  Vars
    
    var recipesResponse: RecipesListModel?
    var dataSource = [Recipe]()
    var recentSearch = [String]()
    var searchKey = ""
    private var from = 0
    private var to = 10
    var shouldPaginate: Bool {
        return recipesResponse?.more ?? false
    }
    
    //MARK: -  functions
    
    func numberOfRows(context: ScreenContext) -> Int {
        switch context {
        case .recentSearch:
            return recentSearch.count
        case .recipeData:
            return dataSource.count
        }
    }
    
    func requestRecipesList(searchKey: String){
        self.searchKey = searchKey
        getRecipesList()
    }

    func requestRecipeCount() {
        to += 10
        from += 10
        getRecipesList()
    }
    
    func clearDataSource() {
        dataSource = [Recipe]()
        from = 0
        to = 10
    }

    private func getRecipesList() {
        let recipesUrl = APIs.Instance.getRecipesSearchResult(searchKey: searchKey.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "", from: from, to: to)
        guard let url = URL(string: recipesUrl) else {
            errorHandler?("Something went worng!!!")
            return
        }
        APIClient.request(url: url, httpMethod: .get, responseType: RecipesListModel.self) { [weak self] response, error in
            guard let response = response, error == nil else {
                self?.errorHandler?(error?.localizedDescription ?? "")
                return
            }
            self?.recipesResponse = response
            if let recipes = response.hits {
                self?.dataSource += recipes
            }
            self?.newDataToDisplayHandler?()
        }
    }
    
    func getRecentSearches() {
        recentSearch = UserDefaults.standard.stringArray(forKey: "recentSearch") ?? []
    }
    
    func addToRecentSearch(newSearchKey: String) {
        if recentSearch.contains(searchKey) == false {
            if recentSearch.count == 10 {
                recentSearch.remove(at: 0)
                addSearchKey(newSearchKey: newSearchKey)
            }else{
                addSearchKey(newSearchKey: newSearchKey)
            }
        }
    }
    
    private func addSearchKey(newSearchKey: String){
        recentSearch.append(newSearchKey)
        UserDefaults.standard.setValue(recentSearch, forKey: "recentSearch")
    }
}
