//
//  ViewController.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/21/21.
//

import UIKit

enum ScreenContext: String{
    case recentSearch
    case recipeData
}

class RecipeSearchViewController: UIViewController {
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var searchTextField: UITextField!
    private var screenContext: ScreenContext = .recentSearch
    private var viewModel = RecipesSearchViewModel()
    private var activityIndicator = ActivityIndicator()
    private var alertView: AlertHandler?
    private var recentSearchCount = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Search"
        configureTableView()
        configureScreenStatus()
        configureSearchTextField()
        bindViewModel()
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func configureSearchTextField() {
        searchTextField.delegate = self
        searchTextField.returnKeyType = UIReturnKeyType.search
    }
    
    private func configureScreenStatus() {
        searchTextField.becomeFirstResponder()
        screenContext = .recentSearch
        viewModel.getRecentSearches()
        tableView.reloadData()
    }
    
    private func requestRecipesList(key: String) {
        activityIndicator.displayForLoading(in: self.view)
        viewModel.requestRecipesList(searchKey: key)
    }
    
    private func bindViewModel() {
        viewModel.errorHandler = { [weak self] message in
            self?.activityIndicator.dismiss()
            self?.alertView?.showErrorMessage(message: message)
        }
        viewModel.newDataToDisplayHandler = { [weak self] in
            self?.activityIndicator.dismiss()
            self?.handleEmptyState()
            self?.tableView.reloadData()
        }
    }
    
    private func handleEmptyState() {
        if viewModel.dataSource.isEmpty == true {
            self.tableView.setEmptyView(message: "No Recipes found")
        } else {
            self.tableView.removeEmptyState()
        }
    }
}
//MARK: -  TableView Delegate

extension RecipeSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == viewModel.dataSource.count - 1, viewModel.shouldPaginate {
            self.tableView.startSpinnerIndicator()
            viewModel.requestRecipeCount()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch screenContext {
        case .recipeData:
            RecipeDetailsConfiguration.navigateToRecipeDetails(recipe: viewModel.dataSource[indexPath.row].recipe ?? RecipeData(), presentingViewController: self)
        case .recentSearch:
            configureRecentSearchSelection(indexPath: indexPath)
        }
    }
    
    private func configureRecentSearchSelection(indexPath: IndexPath){
        requestRecipesList(key: viewModel.recentSearch[recentSearchCount - (indexPath.row + 1)])
        screenContext = .recipeData
        self.searchTextField.text = viewModel.recentSearch[recentSearchCount - (indexPath.row + 1)]
        searchTextField.resignFirstResponder()
        tableView.reloadData()
    }
}
//MARK: -  TableView Delegate

extension RecipeSearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRows(context: screenContext)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch screenContext {
        case .recentSearch:
            return configureRecentSearchCell(indexPath: indexPath)
        case .recipeData:
            return configureRecipeCell(indexPath: indexPath)
        }
    }
    
    private func configureRecipeCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeCell() as RecipeTableViewCell
        cell.setDataToCell(with: viewModel.dataSource[indexPath.row].recipe ?? RecipeData())
        return cell
    }
    
    private func configureRecentSearchCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeCell() as RecentSearchTableViewCell
        recentSearchCount = viewModel.recentSearch.count
        cell.recentSearchKey.text = viewModel.recentSearch[recentSearchCount - (indexPath.row + 1)]
        return cell
    }
}
//MARK: -  TextField Delegate

extension RecipeSearchViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchTextField.resignFirstResponder()
        screenContext = .recipeData
        requestRecipesList(key: self.searchTextField.text ?? "")
        viewModel.addToRecentSearch(newSearchKey: self.searchTextField.text ?? "")
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        screenContext = .recentSearch
        viewModel.clearDataSource()
        self.tableView.reloadData()
        self.tableView.tableFooterView?.isHidden = true
        self.tableView.removeEmptyState()
        return true
    }
}
