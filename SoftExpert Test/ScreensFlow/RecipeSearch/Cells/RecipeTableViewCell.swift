//
//  RecipeTableViewCell.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/21/21.
//

import UIKit
import SDWebImage

class RecipeTableViewCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    @IBOutlet weak var source: UILabel!
    @IBOutlet weak var healthLabels: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureDesign()
    }
    
    func setDataToCell(with data: RecipeData) {
        recipeImage.sd_setImage(with: URL(string: data.image ?? ""), placeholderImage: UIImage(named: "Food"))
        recipeTitle.text = data.title
        source.text = data.source
        healthLabels.text = data.healthLabels?.joined(separator:", ")
    }
    
    private func configureDesign() {
        containerView.setShadow()
    }
}
