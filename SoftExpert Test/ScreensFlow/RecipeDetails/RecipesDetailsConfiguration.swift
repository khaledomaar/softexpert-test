//
//  RecipesDetailsConfiguration.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/22/21.
//

import UIKit

enum RecipeDetailsConfiguration {
    static func navigateToRecipeDetails(recipe: RecipeData, presentingViewController: UIViewController) {
        guard let detailsViewCtrl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecipeDetailsViewController") as? RecipeDetailsViewController else { return }
        detailsViewCtrl.viewModel = RecipeDetailsViewModel(recipeData: recipe)
        presentingViewController.present(detailsViewCtrl, animated: true)
    }
}
