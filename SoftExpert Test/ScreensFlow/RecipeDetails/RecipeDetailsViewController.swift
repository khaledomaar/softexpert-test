//
//  RecipeDetailsViewController.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/22/21.
//

import UIKit
import SafariServices

class RecipeDetailsViewController: UIViewController {
    
    @IBOutlet private var recipeImage: UIImageView!
    @IBOutlet private var recipeTitle: UILabel!
    @IBOutlet private var recipeIngredients: UILabel!
    var viewModel: RecipeDetailsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        setDataToOutlets()
    }
    
    private func setDataToOutlets() {
        recipeImage.sd_setImage(with: URL(string: viewModel?.recipeData?.image ?? ""), placeholderImage: UIImage(named: "Food"))
        recipeTitle.text = viewModel?.recipeData?.title
        recipeIngredients.text = viewModel?.recipeData?.ingredientLines?.joined(separator: "\n")
    }
    
    @IBAction func publisherUrl(_ sender: Any) {
        guard let url = URL(string: viewModel?.recipeData?.recipeUrl ?? "") else { return }
        let vc = SFSafariViewController(url: url)
        present(vc, animated: true)
    }
}
