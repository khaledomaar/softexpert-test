//
//  RecipeDetailsViewModel.swift
//  SoftExpert Test
//
//  Created by Khaled Omar on 5/22/21.
//

import Foundation
import UIKit

class RecipeDetailsViewModel {
    var recipeData: RecipeData?
    
    init(recipeData: RecipeData) {
        self.recipeData = recipeData
    }
}
